import { models } from 'powerbi-client';

export const settings = {
    layoutType:models.LayoutType.Custom,
    customLayout : {
        pageSize :{
            type : models.PageSizeType.Widescreen
        }
    },
    panes: {
        bookmarks: {
            visible: false
        },
        fields: {
            expanded: false
        },
        filters: {
            expanded: false,
            visible: false
        },
        pageNavigation: {
            visible: false
        },
        selection: {
            visible: true
        },
        syncSlicers: {
            visible: true
        },
        visualizations: {
            expanded: false
        }
    }
}

export const embedConfig = {
        type: 'report', // Supported types: report, dashboard, tile, visual, and qna.
        id: '14b6cbde-408e-478a-9a60-7c60eec25a9b',
        embedUrl: '',
        accessToken: '',
        tokenType: models.TokenType.AAD, // Use models.TokenType.Aad if you're embedding for your organization.
        permissions: models.Permissions.All,
        settings: settings
    }

export const eventHandlers = new Map([
        ['loaded', function () {
            console.log('Report loaded');
        }],
        ['rendered', function () {
            console.log('Report rendered');
        }],
        ['error', function (event) {
            console.log(event.detail);
        }]
    ])

export const cssClassName = "Embed-container" 

export const getEmbeddedComponent = (embeddedReport) => {
        window.report = embeddedReport;
    }