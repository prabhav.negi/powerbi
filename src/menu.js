import {useState, useEffect} from 'react'
import Dropdown from './dropdown'
import {settings} from './config'
const Menu = (props) => {
    let className = props.visible?" visible":""
    const panesList = ['bookmarks', 'filters', 'pageNavigation',]
    const pageList = ['Standard','Cortana','Letter','Custom']
    const [visiblePanes, setvisiblePanes] = useState(false)
    const [visiblePage, setvisiblePage] = useState(false)
    const [newSettings, setNewSettings] = useState(settings)

    const updateSetting =  async () => {
        await window.report.updateSettings(newSettings) //updating settings
    }
    useEffect(() => {
        updateSetting()
    }, [newSettings])

    return (
        <div className={"menu"+className}>
            <a onClick={()=>{setvisiblePanes(!visiblePanes)}} >Panes</a>
            {visiblePanes && <Dropdown className={className} newSettings={newSettings} setNewSettings={setNewSettings} panesList={panesList}/> }
            <a onClick={()=>{setvisiblePage(!visiblePage)}} >PageList</a>
           {visiblePage && <Dropdown className={className} newSettings={newSettings} setNewSettings={setNewSettings} pageList={pageList} />}
            {/* <a>Size</a>
            <Dropdown className={className} panesList={panesList} /> */}
        </div>
    )
}

export default Menu