import {useState, useEffect} from 'react'
import { models } from 'powerbi-client';
const Dropdown = (props) => {
    const { panesList, className, pageList, newSettings,setNewSettings} = props
    const [customLayout, setCustomLayout] = useState(newSettings.customLayout)

    const setLayout = (e) => {
        let panes = newSettings.panes
        let key = e.target.getAttribute('name')
        let toChange =  newSettings.panes[key]
        let value = newSettings.panes[key].visible
        toChange = {...toChange,visible:!value} 
        panes = {...panes,[key]:toChange}
        setNewSettings(newSettings => ({...newSettings,panes:panes}))
        // console.log("done")
    }
    const updatePages = (e) => {
        let value = models.PageSizeType[e.target.getAttribute('name')]
        if (customLayout.pageSize.type == value) {
            setCustomLayout(customLayout => ({...customLayout,pageSize:{type:0}}))    
            return
        }
        let pagesize = {type:value}
        setCustomLayout(customLayout => ({...customLayout,pageSize:pagesize}))
    }
    const setSetting = async () => {
        console.log(customLayout)
        if(customLayout.pageSize.type != 0)
            setNewSettings(newSettings => ({...newSettings,customLayout:customLayout}))
        else
            setNewSettings(newSettings => ({...newSettings,customLayout:customLayout})) 
        }
    useEffect (()=> {
        setSetting()
    },[customLayout])

    return (
        <div className={className?"dropdown dvisible":"dropdown"}>
            {panesList && panesList.map((item) => (
                <p key={item} onClick={setLayout} name={item}>{item}</p> 
            ))}
             {pageList && pageList.map((item) => (
                <p key={item} onClick={updatePages} name={item}>{item}</p> 
            ))}
        </div>
    )
}

export default Dropdown