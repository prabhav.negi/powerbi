import { PowerBIEmbed } from 'powerbi-client-react';
import {embedConfig, eventHandlers, cssClassName, getEmbeddedComponent} from './config'
import Navbar from './Navabar'
import './App.css'
import {useEffect, useState} from 'react'
import { authenticate } from './getToken/getToken';

function App() {
  const [isLoading, setisLoading] = useState(true)
  const getToken = async () => {
    const [data,error] = await authenticate()
    console.log(data)
    if(error)
      console.log(error)
    else if (data)
      embedConfig.embedUrl = data.embedURL
      embedConfig.accessToken = data.accessToken
    setisLoading(false)
  }

  useEffect(() => {
   getToken()
  },[])

  return (
    <div className="App"> 
      {!isLoading && <Navbar/>}
      {!isLoading &&  <PowerBIEmbed
        embedConfig = {embedConfig}
        eventHandlers = {eventHandlers}
        cssClassName = {cssClassName}
        getEmbeddedComponent = {getEmbeddedComponent}
      /> }
    </div>
  );
}

export default App;
