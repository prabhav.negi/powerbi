import React, { useState } from 'react';
import Menu from './menu'
const Navbar = () => {
    const [isVisible, setisVisible] = useState(false)
    const action = (e) => {
        if(e.target.value === 'save') {
            window.report.save()
            return
        }
        window.report.switchMode(e.target.value)
    }
    return (
        <div className="navbar">
            <p>POWER B.I. </p>
            <div>
                <button type="button" value="view" onClick={action}>View</button>
                <button type="button" value="save" onClick={()=>setisVisible(!isVisible)}>Layout</button>
                <button type="button" value="edit" onClick={action}>Edit</button>
                <button type="button" value="save" onClick={action}>Save</button>
                <Menu visible={isVisible}/>

            </div>
        </div>
    )
}

export default Navbar