export const scopes = ["https://analysis.windows.net/powerbi/api/Report.Read.All"];

// Client Id (Application Id) of the AAD app.
export const clientId = "367209bc-3df6-46ad-83cb-46daef03b729";

// Id of the workspace where the report is hosted
export const workspaceId = "3a926743-74a6-46cf-8cda-4c01620896a3";

// Id of the report to be embedded
export const reportId = "14b6cbde-408e-478a-9a60-7c60eec25a9b";