import * as tokenConfig from './tokenConfig'
import * as msal from "@azure/msal-browser"


const getEmbedURL = async (accessToken) => {
    console.log("here")
    try {
        const response =  await fetch("https://api.powerbi.com/v1.0/myorg/groups/" + tokenConfig.workspaceId + "/reports/" + tokenConfig.reportId, {
                                headers: { "Authorization": "Bearer " + accessToken },  method: "GET"
                            })
        if(response.ok) {
            try {
                const {embedUrl} = await response.json()
                return embedUrl
            }
            catch (err) {
                throw err
            }
        }
        else
            throw new Error(response.status,"error in embedUrl response")
    }
    catch(err) {
        throw err
    }
}

export const authenticate = async () => {

    let data = null
    let error = null

    const msalConfig = {
        auth: {
            clientId: tokenConfig.clientId,
            redirectUri: ''
        }
    }
    let loginRequest = {
        scopes: tokenConfig.scopes
    };
    const HandleLogin = async () => {
        console.log("handle login")
        async function handleResponse(response) {
                console.log(response)
                msalInstance.acquireTokenRedirect(loginRequest);
        }
        msalInstance.handleRedirectPromise().then(handleResponse).catch(err => console.log(err));
    }
    
    const msalInstance = new msal.PublicClientApplication(msalConfig);

    if(msalInstance.getAllAccounts().length != 0) {
        console.log("userexist")
        // msalInstance.handleRedirectPromise().then(res => msalInstance.logoutRedirect()).catch(err => console.log(err))

        try {
            loginRequest = {...loginRequest,account: msalInstance.getAllAccounts()[0]}
            const response =  await msalInstance.acquireTokenSilent(loginRequest)
            const accessToken = response.accessToken;
            const username = response.account.username
            const embedURL = await getEmbedURL(accessToken)
            data = {accessToken, username, embedURL}
        }
        catch (err) {
            console.log(err)
            if (err instanceof msal.InteractionRequiredAuthError)
                await HandleLogin()
            else
                error = err.toString()
        }
    }
    else
        await HandleLogin()
        
    return [data,error]
}
